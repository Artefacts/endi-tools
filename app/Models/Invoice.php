<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Invoice extends Model
{
    const TABLENAME = 'invoices';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = self::TABLENAME;

    public $incrementing = false;

    /**
     * The attributes that aren't mass assignable.
     * To make all of your attributes mass assignable,
     * you may define your model's $guarded property as an empty array.
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast.
     * Cast dates to make isDirty() working fine.
     * @var array
     */
    protected $casts = [
    ];

    const TYPE_INVOICE = 'invoice';
    const TYPE_INTERNALINVOICE = 'internalinvoice';
    const TYPE_CANCELINVOICE = 'cancelinvoice';
    const TYPE_INTERNALCANCELINVOICE = 'internalcancelinvoice';
    const TYPES = [
        self::TYPE_INVOICE,self::TYPE_INTERNALINVOICE, self::TYPE_CANCELINVOICE, self::TYPE_INTERNALCANCELINVOICE
    ];
    const TYPES_CANCELED = [
        self::TYPE_CANCELINVOICE, self::TYPE_INTERNALCANCELINVOICE
    ];

    public static function isTypeCanceled( $type )
    {
        if( ! in_array($type,self::TYPES ))
            throw new \InvalidArgumentException('Unknow type "'.$type.'"');
        return in_array($type, self::TYPES_CANCELED );
    }

}
