<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{

    const TABLENAME = 'quotes';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = self::TABLENAME;
}
