<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * We need personnal data to permit match between different systems like enDI, pay ...
 *
 * @property int $id
 * @property string $ss_id_hash,
 * @property int $endi_id, enDI CSV "Identifiant enDI"
 * @property Carbon endi_created_at, enDI CSV "Créé(e) le"
 * @property Carbon endi_updated_at, enDI CSV "Mis(e) à jour le"
 * @property string status, enDI CSV "Situation actuelle dans la CAE"
 * @property string home_branch, enDI CSV "Antenne de rattachement"
 * @property string courtesy, enDI CSV "Civilité"
 * @property string sexe, enDI CSV "Sexe"
 * @property Carbon birthdate, enDI CSV "Date de naissance"
 * @property string lastname, enDI CSV "Nom"
 * @property string firstname, enDI CSV "Prénom"
 * @property string birthname, enDI CSV "Nom de naissance"
 * @property string email1, enDI CSV "E-mail 1"
 * @property string email2, enDI CSV "E-mail 2"
 * @property string address, enDI CSV "Adresse"
 * @property string city_postcode, enDI CSV "Code postal"
 * @property string city, enDI CSV "Ville"
 * @property string endi_analytical_1, enDI CSV "Compte_analytique 1"
 * @property string endi_analytical_2, enDI CSV "Compte_analytique 2"
 * @property string endi_analytical_3, enDI CSV "Compte_analytique 3"
 * @property string endi_analytical_4, enDI CSV "Compte_analytique 4"
 * @property string endi_analytical_5, enDI CSV "Compte_analytique 5"
 * @property string endi_analytical_6, enDI CSV "Compte_analytique 6"
 */
class Company extends Model
{
    const TABLENAME = 'companies';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = self::TABLENAME;

    public $incrementing = false;

    /**
     * The attributes that aren't mass assignable.
     * To make all of your attributes mass assignable,
     * you may define your model's $guarded property as an empty array.
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast.
     * Cast dates to make isDirty() working fine.
     * @var array
     */
    protected $casts = [
        'endi_created_at' => 'datetime',
        'endi_updated_at' => 'datetime',
        'birthdate' => 'date',
    ];
}
