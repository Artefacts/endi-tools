<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Estimation extends Model
{
    const TABLENAME = 'estimations';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = self::TABLENAME;

    public $incrementing = false;

    /**
     * The attributes that aren't mass assignable.
     * To make all of your attributes mass assignable,
     * you may define your model's $guarded property as an empty array.
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast.
     * Cast dates to make isDirty() working fine.
     * @var array
     */
    protected $casts = [
    ];
}
