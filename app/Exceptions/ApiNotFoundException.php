<?php

namespace App\Exceptions;

class ApiNotFoundException extends ApiException
{
    public function __construct($message)
    {
        parent::__construct('Not found: "'.$message.'"');
    }
}
