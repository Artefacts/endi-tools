<?php

namespace App\Exceptions;

class ApiException extends \Exception
{
    public function __construct($message)
    {
        parent::__construct('Api error: '.$message);
    }
}
