<?php

namespace App\Services;

use App\Exceptions\ApiNotFoundException;
use Carbon\Carbon;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Psr\Http\Message\StreamInterface;

class ApiClient
{

	/**
	 *
	 * @var \GuzzleHttp\Client
	 */
	protected $client;

	protected $sleep;

	/**
	 * Initialize the HTTP Client
	 * and do Login.
	 * 
	 * @todo Manage login failed.
	 * 
	 * @param mixed $url_base 
	 * @param mixed $username 
	 * @param mixed $password 
	 * @return void 
	 * @throws GuzzleException 
	 */
	public function __construct($url_base, $username, $password)
	{
		if (!$this->client) {
			Log::info(__METHOD__, ['username' => $username, 'base_url' => $url_base]);

			$this->sleep = Config::get('endi-tools.http_sleep');

			$cookies = new CookieJar();
			/**
			 * Navigateur non supporté - enDI -> set the User-Agent
			 * Auth works with cookie "beaker.session.id"
			 */
			$client = new Client([
				'base_uri' => $url_base,
				'cookies' => $cookies,
				'headers' => [
					'User-Agent' => Config::get('endi-tools.web_user_agent'),
					'Content-type' => 'text/html',
				],
			]);
			$response = $client->post(
				'/login',
				[
					'form_params' => [
						// Some params are mandatory !
						'submit' => 'submit',
						//'nextpage'=>'/',
						//'_charset_'=>'UTF-8',
						//'__formid__' => 'authentication',
						'login' => $username,
						'password' => $password,
					],
				]
			);
			//$this->comment( $output, 'code: '.$response->getStatusCode() );
			//$this->comment( $output, 'reason: '.$response->getReasonPhrase() );
			//var_export( $cookies );

			$this->client = new Client([
				'base_uri' => $url_base,
				'cookies' => $cookies,
				'headers' => [
					'User-Agent' => Config::get('endi-tools.api_user_agent'),
					'Content-type' => 'application/json',
				],
			]);

		}
	}

	public function getClient()
	{
		return $this->client;
	}

	public static function isError(ResponseInterface $response)
	{
		/*
        $this->comment($output, 'code: ' . $response->getStatusCode());
        $this->comment($output, 'reason: ' . $response->getReasonPhrase());
        //var_export( $response->getHeaders() );
        $this->comment($output, 'content-type: '.$response->getHeader('Content-Type')[0] );
        // text/html; charset=UTF-8
        */
		if ($response->getStatusCode() != 200)
			return true;
		if ($response->getHeader('Content-Type')[0] != 'application/json')
			return true;
		return false;
	}

	/**
	 * HTTP GET an URL.
	 * Use a file cache if configured.
	 * 
	 * @todo When HTTP PUT will be implemented, the cache filename must be refactorized to be shared.
	 * 
	 * @param mixed $url 
	 * @return StreamInterface|string 
	 * @throws GuzzleException 
	 * @throws ApiNotFoundException 
	 */
	public function getJson($url)
	{
		$cache_active = Config::get('endi-tools.cache.active');
		$file = null;
		$data = null;
		if ($cache_active) {
			$file = Config::get('endi-tools.cache.folder') . '/' . Str::slug($url);
			if (file_exists($file)) {
				$fstat = stat($file);
				if ($fstat['mtime'] > (new Carbon())->subSeconds(Config::get('endi-tools.cache.ttl'))->getTimestamp()) {
					Log::debug(__METHOD__ . ' Read from Cache ' . $url);
					$data = file_get_contents($file);
				}
			}
		}

		if (! $data) {
			Log::debug(__METHOD__ . ' Read from Http ' . $url);
			$response = $this->getClient()->get($url);
			if (ApiClient::isError($response))
				throw new ApiNotFoundException($url);
			$data = $response->getBody();

			if ($cache_active) {
				file_put_contents($file, $data);
			}

			if ($this->sleep > 0) {
				usleep(1000 * $this->sleep);
			}
		}

		return json_decode($data);
	}
}
