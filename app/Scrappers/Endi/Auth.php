<?php

namespace App\Scrappers\Endi;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class Auth
{
    public function login($user, $password)
    {
        /**
         * @var \GuzzleHttp\Client $http
         * @var \Psr\Http\Message\ResponseInterface $response
         */

        $http = App::make(\GuzzleHttp\Client::class);

        $response = $http->post('/login', [
            // important !
            'allow_redirects' => true,
            'form_params' => [
                '__formid__' => 'authentication',
                'login' => $user,
                'password' => $password,
                'remember_me' => true,
                'nextpage' => '/',
                'submit' => 'submit'
            ]
        ]);
        if ($response->getStatusCode() != Response::HTTP_OK)
            throw new \RuntimeException(__METHOD__ . ' failed, status:' . $response->getStatusCode());
    }
}
