<?php

namespace App\Scrappers\Endi;

use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use PHPHtmlParser\Dom;

abstract class Scrapper
{

    /**
     * @var \GuzzleHttp\Client
     */
    protected $http;

    /**
     * @var bool
     */
    protected $cache_pages;

    public function __construct(HttpClient $http)
    {
        $this->http = $http;
    }

    abstract protected function processPage(Dom $dom, bool $isLastPage);

    protected function slurpDom(array $queryString)
    {
        /**
         * @todo Avoid infinite loop with some max pages limit
         */
        $page = 0;
        do {
            $queryString['page'] = ++$page;

            $cache_filepath = \storage_path('app/cache/' . Str::slug(get_class($this)) . '-' . $page . '.html');

            if (!$this->cache_pages || !file_exists($cache_filepath)) {
                /**
                 * @var \Psr\Http\Message\ResponseInterface $response
                 */
                $response = $this->http->get('/estimations', [
                    'query' => $queryString,
                ]);
                if ($response->getStatusCode() != Response::HTTP_OK) {
                    throw new \RuntimeException(__METHOD__ . ' failed, status:' . $response->getStatusCode());
                }
                $content = $response->getBody();
                if ($this->cache_pages) {
                    file_put_contents($cache_filepath, $content);
                    //Storage::disk('local')->put('cache/'.$cache_page, $content);
                }

            } else {
                $content = file_get_contents($cache_filepath);
            }

            $dom = new Dom();
            $dom->loadStr($content);

            // Pager

            $items = $dom->find('#target_content div.pager > *');
            if ((!$items) || (count($items) == 0))
                throw new \RuntimeException(__METHOD__ . ' parsing failed.');

            $isLastPage = static::isLastPage($items);

            $this->processPage($dom, $isLastPage);

            if ($isLastPage) {
                //echo $page, '.',"\n";
                break;
            } else {
                //echo $page, '...',"\n";
            }

        } while (true);
    }

    public static function isLastPage(\PHPHtmlParser\Dom\Node\Collection $pagerNode)
    {
        // echo get_class($pagerNode), ' items count:', count($pagerNode), "\n";
        $current = 0;
        $tagCounter = 0;
        foreach ($pagerNode as $item) {
            switch ($item->tag->name()) {
                case 'text':
                    break;
                default:
                    $tagCounter++;
                    // echo 'tag: ',$item->tag->name(),' ', $item->tag->getAttribute('class')->getValue(), "\n";
                    if ($item->tag->getAttribute('class')->getValue() == 'current')
                        $current = $tagCounter;
            }
        }
        // echo $current, ' ', $tagCounter,"\n"; 
        if ($current == $tagCounter)
            return true;
        return false;
    }

}
