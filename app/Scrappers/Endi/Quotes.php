<?php

namespace App\Scrappers\Endi;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client as HttpClient;
use Symfony\Component\HttpFoundation\Response;
use PHPHtmlParser\Dom;
use Illuminate\Support\Str;
use Ramsey\Uuid\Type\Integer;
use \PHPHtmlParser\Dom\Node\HtmlNode;

class Quotes extends Scrapper
{

    /**
     * 500 per page = 10 pages for 4.594 results.
     */
    const ITEMS_PER_PAGE = 500;
    /**
     * Seconds to sleep after each page.
     */
    const SLEEP_AFTER_PAGE = 2;

    const STATUS_UNVALIDATED = 'unvalidated';
    const STATUS_SIGNED = 'signed';
    const STATUS_ABORTED = 'aborted';
    const STATUS_SENT = 'sent';
    const STATUS_WAITING = 'waiting';

    public function __construct(HttpClient $http)
    {
        parent::__construct($http);

        $this->cache_pages = \env('ENDITOOLS_SCRAPPER_ENDI_QUOTES_CACHE_PAGE', false);
    }

    public function loadAndUpdateDb()
    {
        $this->slurpDom([
            /*
            https://endi.ma.coop/estimations?_charset_=UTF-8
            &__formid__=deform
            &year=-1
            &signed_status=all
            &doctype=both&__start__=ttc%3Amapping&start=
            &end=&__end__=ttc%3Amapping&__start__=period%3Amapping&__start__=start%3Amapping&date=&__end__=start%3Amapping
            &__start__=end%3Amapping&date=&__end__=end%3Amapping&__end__=period%3Amapping
            &company_id=&customer_id=&business_type_id=all
            &search=
            &items_per_page=10
            &submit=submit
            */
            '__formid__' => 'deform', 'submit' => 'submit',
            'signed_status' => 'all', 'doctype' => 'both',
            'start' => '', '__start__' => 'ttc:mapping', '__start__' => 'period:mapping', '__start__' => 'start:mapping', '__start__' => 'end:mapping',
            'end' => '', '__end__' => 'ttc:mapping', '__end__' => 'period:mapping', '__end__' => 'start:mapping', '__end__' => 'end:mapping',
            'date' => '', 'search' => '', 'year' => -1,
            'company_id' => '', 'customer_id' => '', 'business_type_id' => 'all',
            'items_per_page' => self::ITEMS_PER_PAGE,
            'page' => 0,
        ]);
    }

    protected function processPage(Dom $dom, bool $isLastPage)
    {
        // Quotes

        //file_put_contents('toto.html', $response->getBody() );
        //return ;

        $rows = $dom->find('.table_container table tbody tr');
        foreach ($rows as $row) {
            // First body's row is for totals
            $class = $row->tag->getAttribute('class')->getValue();
            if ($class == 'row_recap')
                continue;
            $this->processQuote($row);
        }

        sleep(self::SLEEP_AFTER_PAGE);
    }

    protected function processQuote(HtmlNode $row)
    {
        $status = $this->retrieveStatus($row);
        $quote_id = $this->retrieveId($row);
        $company_id = $this->retrieveCompany($row);
        $customer_id = $this->retrieveCustomer($row);
        echo $status, ' / ', $quote_id, ' / ', $company_id, ' / ', $customer_id, "\n";
    }

    protected function retrieveStatus(HtmlNode $row): string
    {
        // Quote status
        $class = $row->tag->getAttribute('class')->getValue();

        $status = null;
        if (strpos($class, 'status-valid') == false)
            return self::STATUS_UNVALIDATED;
        else if (strpos($class, 'signed-status-signed'))
            return self::STATUS_SIGNED;
        else if (strpos($class, 'signed-status-waiting'))
            return self::STATUS_WAITING;
        else if (strpos($class, 'signed-status-aborted'))
            return self::STATUS_ABORTED;
        else if (strpos($class, 'signed-status-sent'))
            return self::STATUS_SENT;

        throw new \RuntimeException(__METHOD__ . ' Unknow status: ' . $class);
    }

    protected function retrieveId(HtmlNode $row): int
    {
        // Quote ID

        $quote_id = -1;
        $nodes = $row->find('td.col_actions .btn-group a');
        foreach ($nodes as $node) {
            if (preg_match('#^/estimations/(\d+)$#', $node->tag->getAttribute('href')->getValue(), $m)) {
                $quote_id = \intval($m[1]);
                break;
            }
        }
        return $quote_id;
    }

    protected function retrieveCompany(HtmlNode $row): int
    {
        $company_id = -1;
        // marche pas :(
        //$nodes = $row->find('td[2] a');
        $nodes = $row->find('td a');
        foreach ($nodes as $node) {
            $href = $node->tag->getAttribute('href')->getValue();
            if (preg_match('#^/companies/(\d+)$#', $href, $m)) {
                $company_id = \intval($m[1]);
                break;
            }
        }
        return $company_id;
    }

    protected function retrieveCustomer(HtmlNode $row): int
    {
        $customer_id = -1;
        // marche pas :(
        //$nodes = $row->find('td[5] a');
        $nodes = $row->find('td a');
        foreach ($nodes as $node) {
            $href = $node->tag->getAttribute('href')->getValue();
            if (preg_match('#^/customers/(\d+)$#', $href, $m)) {
                $customer_id = \intval($m[1]);
                break;
            }
        }
        return $customer_id;
    }
}
