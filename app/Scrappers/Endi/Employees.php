<?php

namespace App\Scrappers\Endi;

use App\Models\Company;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Str;
use Cyrille37\PhpCsvReader\Reader as CsvReader;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class Employees
{
    const CSV_SEPARATOR = ';';

    /**
     * Mapping of CSV columns to Model attributes for creating or updating Company Model.
     * @var array
     */
    const FileColumnsToModelAttributes = [
        'Identifiant enDI' => 'id',
        'Créé(e) le' => 'endi_created_at',
        'Mis(e) à jour le' => 'endi_updated_at',
        'Situation actuelle dans la CAE' => 'status',
        'Antenne de rattachement' => 'home_branch',
        'Civilité' => 'courtesy',
        'Sexe' => 'sexe',
        'Date de naissance' => 'birthdate',
        'Nom' => 'lastname',
        'Prénom' => 'firstname',
        'Nom de naissance' => 'birthname',
        'E-mail 1' => 'email1',
        'E-mail 2' => 'email2',
        'Adresse' => 'address',
        'Code postal' => 'city_postcode',
        'Ville' => 'city',
        'Compte_analytique 1' => 'endi_analytical_1',
        'Compte_analytique 2' => 'endi_analytical_2',
        'Compte_analytique 3' => 'endi_analytical_3',
        'Compte_analytique 4' => 'endi_analytical_4',
        'Compte_analytique 5' => 'endi_analytical_5',
        'Compte_analytique 6' => 'endi_analytical_6',
    ];

    /**
     * @var bool $cache_csv
     */
    protected $cache_csv;

    /**
     * @var \GuzzleHttp\Client
     */
    protected $http;

    public function __construct(HttpClient $http)
    {
        $this->http = $http;

        $this->cache_csv = \env('ENDITOOLS_SCRAPPER_ENDI_EMPLOYEES_CACHE_CSV', false);
    }

    public function loadAndUpdateDb()
    {
        $filepath = \storage_path('app/cache/' . Str::slug(get_class($this)) . '.csv');

        if (!$this->cache_csv || !file_exists($filepath)) {

            /**
             * @var \Psr\Http\Message\ResponseInterface $response
             */
            $response = $this->http->get('/userdatas.csv');

            /*
            echo 'Status:' . $response->getStatusCode(), "\n",
            'Headers:' . print_r($response->getHeaders(),true), "\n",
            $response->getBody() ,"\n";
            Location] => Array
            (
                [0] => https://endi.ma.coop/jobs/230
            )
            */

            if ($response->getStatusCode() != Response::HTTP_FOUND) {
                throw new \RuntimeException(__METHOD__ . ' failed, status:' . $response->getStatusCode());
            }

            $jobUrl = $response->getHeader('Location')[0];
            echo 'jobUrl: ', $jobUrl, "\n";

            /**
             * @todo Add a timeout
             */
            $running = true;
            $result_filename = null;
            do {
                $response = $this->http->get($jobUrl, [
                    'headers' => [
                        'Accept' => 'application/json, text/javascript, */*; q=0.01',
                        'X-Requested-With' => 'XMLHttpRequest',
                    ],
                ]);

                /*
                echo 'Status:' . $response->getStatusCode(), "\n",
                    'Headers:' . print_r($response->getHeaders(),true), "\n",
                    $response->getBody() ,"\n";
                { "label": "G\u00e9n\u00e9ration de fichier", "jobid": "58bbea6f-6d05-4a4d-a80e-9bf893fa22e0",
                "status": "running", "created_at": "30/09/2022 \u00e0 19:23", "updated_at": "30/09/2022 \u00e0 19:23",
                "filename": null, "error_messages": null}
                {"label": "G\u00e9n\u00e9ration de fichier", "jobid": "6717099d-2202-49d9-a29a-0aeb7ab61efd",
                "status": "completed", "created_at": "30/09/2022 \u00e0 19:25", "updated_at": "30/09/2022 \u00e0 19:25",
                "filename": "gestion_socialef4kz10tw.csv", "error_messages": null}
                */

                $data = json_decode($response->getBody());
                if ($data->status == 'completed') {
                    $running = false;
                    $result_filename = $data->filename;
                } else {
                    echo "\t", 'Waiting "' . $data->status, '"', "\n";
                    sleep(2);
                }
            } while ($running);

            /*
            https://endi.ma.coop/cooked/gestion_sociale1a2bcd3e.csv
            */
            $response = $this->http->get(\env('ENDITOOLS_SITE') . '/cooked/' . $result_filename);

            file_put_contents($filepath, $response->getBody());

            $this->updateDb($filepath);

            if (!$this->cache_csv) {
                // Remove temporary file
                unlink($filepath);
                //Storage::disk('local')->delete('cache/'.$this->cache_csv_filename );
            }
        } else {
            Log::debug(__METHOD__ . ' use cache_csv_file');

            $this->updateDb($filepath);
        }
    }

    protected function updateDb($filepath)
    {
        //echo __METHOD__, ' file: ', $filename, "\n";

        $csv = new CsvReader($filepath, self::CSV_SEPARATOR);

        $missing = $csv->findMissingHeaders(array_keys(self::FileColumnsToModelAttributes));
        if (!empty($missing)) {
            throw new \RuntimeException('File missing columns "' . implode(',', $missing) . '"');
        }

        // DB datetime has not microseconds but Carbon has,
        // So must set microseconds to zero to make datatime equals works.
        $now = (new Carbon())->startOfSecond();
        $stats = [
            'count' => 0,
            'created' => 0,
            'updated' => 0,
        ];
        foreach ($csv->getRows() as $row) {
            $stats['count']++;
            //echo 'count:', $stats['count'], "\n";
            //echo 'R:',print_r($row,true),"\n";
            $data = [];
            foreach (self::FileColumnsToModelAttributes as $kCsv => $kMod) {
                $value = null;
                switch ($kMod) {
                    case 'endi_created_at':
                    case 'endi_updated_at':
                    case 'birthdate':
                        if ($row[$kCsv])
                            $value = Carbon::createFromFormat('d/m/Y', $row[$kCsv])->setTime(0, 0);
                        break;
                    case 'id':
                        $value = str_replace([' ',' '],'', $row[$kCsv]);
                        break;
                    default:
                        $value = $row[$kCsv];
                }
                $data[$kMod] = $value;
            }

            Log::debug('Processing company ID:'.$data['id']);
            $company = Company::updateOrCreate(
                ['id' => $data['id']],
                $data
            );

            if ($company->wasRecentlyCreated)
                $stats['created']++;
            else if ($company->updated_at->greaterThanOrEqualTo($now))
                $stats['updated']++;
        }
        Log::info(__METHOD__, [$stats]);
    }
}
