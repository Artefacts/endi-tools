<?php

namespace App\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\GuzzleHttp\Client::class, function ($app) {
            return new \GuzzleHttp\Client(
                [
                    'base_uri' => Config::get('endi-tools.base_url'),
                    'allow_redirects' => false,
                    'cookies' => true,
                    'headers' => [
                        'User-Agent' => env('ENDITOOLS_USERAGENT', 'artefacts/endi-tools'),
                        'DNT' => '1',
                        'Referer' => Config::get('endi-tools.base_url'),
                        'Accept' => 'text/html,application/xhtml+xml,application/xml',
                        'Accept-Encoding' => 'gzip, deflate, br',
                        'Accept-Language' => 'fr-FR;q=0.9,fr;q=0.8,en-US;q=0.7,en',
                        'Upgrade-Insecure-Requests' => '1',
                    ],
                ]
            );
        });

        $this->app->singleton(\App\Services\ApiClient::class, function ($app) {
            return new \App\Services\ApiClient(
                Config::get('endi-tools.base_url'),
                Config::get('endi-tools.username'),
                Config::get('endi-tools.password')
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
