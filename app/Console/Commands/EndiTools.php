<?php
namespace App\Console\Commands;

use App\Scrappers\Endi\Auth;
use App\Scrappers\Endi\Quotes;
use App\Scrappers\Endi\Companies;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;


class EndiTools extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'enditools:scrape';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'EndiTools ...';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        throw new \RuntimeException('ABORTED -> DEPRECATED');
        Log::debug(__METHOD__,['app_name'=>\env('APP_NAME'),'base_uri' => Config::get('endi-tools.base_url')]);

        /**
         * @var \GuzzleHttp\Client $http
         */

        $http = App::make(\GuzzleHttp\Client::class);

        (new Auth($http))->login( \env('ENDITOOLS_USER'), \env('ENDITOOLS_PWD') );

        //(new Companies($http))->loadAndUpdateDb();

        $quotes = new Quotes($http);
        $quotes->loadAndUpdateDb();

    }
}