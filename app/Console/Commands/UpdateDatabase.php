<?php

namespace App\Console\Commands;

use App\Exceptions\ApiNotFoundException;
use App\Models\Company;
use App\Models\Estimation;
use App\Models\Invoice;
use App\Models\Project;
use App\Scrappers\Endi\Companies;
use App\Services\ApiClient;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;
use RuntimeException;

class UpdateDatabase extends Command
{
	/**
	 * @var string
	 */
	protected $signature = 'enditools:updateDatabase';

	/**
	 * @var string
	 */
	protected $description = 'Update the database from enDI API.';

	/**
	 * 
	 * @var ApiClient
	 */
	protected $apiClient;

	protected $stats = [
		'start_at' => 0,
		'end_at' => 0,
		'ellapsed' => 0,
		'companies' => [
			'created' => 0,
			'updated' => 0
		],
		'projects' => [
			'created' => 0,
			'updated' => 0
		],
	];

	public function __construct(
		ApiClient $apiClient
	) {
		parent::__construct();
		$this->apiClient = $apiClient;
	}

	/**
	 * @return int
	 */
	public function handle()
	{
		$this->info('Updating database');

		$this->stats['start_at'] = new Carbon();

		$this->loadCompanies();
		$this->loadProjects();
		$this->loadProjectsData();

		$this->stats['end_at'] = new Carbon();
		$this->stats['ellapsed'] = $this->stats['end_at']->diffForHumans($this->stats['start_at']);
		$this->stats['end_at'] = $this->stats['end_at']->toDateTimeString();
		$this->stats['start_at'] = $this->stats['start_at']->toDateTimeString();

		$this->info('Stats: ' . var_export($this->stats, true));
	}

	/**
	 * L'API demanderait beaucoup trop d'appels et ne contient pas le `updated_at`.
	 * On utilise donc le scrapper qui récupère le fichier export de gestion sociale `userdatas.csv`.
	 * @return void
	 * @throws BindingResolutionException
	 * @throws GuzzleException
	 * @throws RuntimeException
	 * @throws InvalidArgumentException
	 */
	protected function loadCompanies()
	{
		$this->info('loading companies...');

		$companiesJson = $this->apiClient->getJson(
			'/api/v1/companies',
		);

		// DB datetime has not microseconds but Carbon has,
		// So must set microseconds to zero to make datatime equals works.
		$now = (new Carbon())->startOfSecond();

		foreach ($companiesJson as $companyJson) {
			/* (object) array(
				'id' => 70430,
				'name' => 'ZZ2-PMN 2023',
			)*/
			//$this->comment(var_export($company));
			$company = Company::updateOrCreate(
				['id' => $companyJson->id],
				['name' => $companyJson->name]
			);
			if ($company->wasRecentlyCreated)
				$this->stats['companies']['created']++;
			else if ($company->updated_at->greaterThanOrEqualTo($now))
				$this->stats['companies']['updated']++;
		}
	}

	protected function loadProjects()
	{
		$this->info('loading projects...');

		foreach (Company::all() as $company) {
			//$this->comment("\t" . 'company ' . $company->id);

			$projectsJson = $this->apiClient->getJson(
				'/api/v1/companies/' . $company->id . '/projects',
			);

			// DB datetime has not microseconds but Carbon has,
			// So must set microseconds to zero to make datatime equals works.
			$now = (new Carbon())->startOfSecond();

			foreach ($projectsJson as $projectJson) {

				$project = Project::updateOrCreate(
					['id' => $projectJson->id],
					[
						'company_id' => $company->id,
						'name' => $projectJson->name,
						'description' => $projectJson->description,
					]
				);
				if ($project->wasRecentlyCreated)
					$this->stats['projects']['created']++;
				else if ($project->updated_at->greaterThanOrEqualTo($now))
					$this->stats['projects']['updated']++;
			}
		}
	}

	protected function loadProjectsData()
	{
		$this->info('loading projectsData...');

		foreach (Project::all() as $project) {
			$this->comment("\t" . 'company '.$project->company_id.' project ' . $project->id);

			$projectJson = $this->apiClient->getJson(
				'/api/v1/projects/' . $project->id . '/tree',
			);

			foreach ($projectJson->estimations as $estimationJson) {
				$this->loadEstimation($estimationJson->id);
			}
			foreach ($projectJson->businesses as $businessJson) {
				// Estimations
				foreach ($businessJson->estimations as $estimationJson) {
					$this->loadEstimation($estimationJson->id);
				}
				// Invoices
				// Not canceled invoices
				foreach ($businessJson->invoices as $invoiceJson) {
					if( ! Invoice::isTypeCanceled($invoiceJson->type_) )
						$this->loadInvoice($invoiceJson);
				}
				// Canceled invoices
				foreach ($businessJson->invoices as $invoiceJson) {
					if( Invoice::isTypeCanceled($invoiceJson->type_) )
						$this->loadInvoice($invoiceJson);
				}
			}
		}
	}

	/**
	 * To avoid duplicate loading of some items
	 * @var array{estimations: array}
	 */
	protected $cache = [
		'estimations' => [],
	];

	protected function loadEstimation($estimation_id)
	{
		if (in_array($estimation_id, $this->cache['estimations']))
			return;
		$this->comment("\t\t" . 'estimation '.$estimation_id);

		$estimationJson = $this->apiClient->getJson(
			'/api/v1/estimations/' . $estimation_id
		);
		$estimation = Estimation::updateOrCreate(
			['id' => $estimation_id],
			[
				'project_id' => $estimationJson->project_id,
				'status'  => $estimationJson->status,
				'name' => $estimationJson->name,
				'description' => $estimationJson->description,
				'date' =>$estimationJson->date,

				'internal_number' => $estimationJson->internal_number,
				'payment_conditions' => $estimationJson->payment_conditions,
				'start_date' => $estimationJson->start_date,
				'end_date' => $estimationJson->end_date,
				'total_ht' => $estimationJson->ht,
				'total_tva' => $estimationJson->tva,
				'total_ttc' => $estimationJson->ttc,
				'payment_lines_count' => count($estimationJson->payment_lines),
				'endi_created_at' => $estimationJson->created_at,
				'endi_updated_at' => $estimationJson->updated_at,
			]
		);
		$this->cache['estimations'][] = $estimation_id;
	}

	protected function loadInvoice($invoiceResumeJson)
	{
		$this->comment("\t\t" . 'invoice '.$invoiceResumeJson->id);

		$canceled = false ;
		if( Invoice::isTypeCanceled($invoiceResumeJson->type_) )
		{
			$canceled = true ;
			$invoiceJson = $this->apiClient->getJson(
				'/api/v1/cancelinvoices/' . $invoiceResumeJson->id
			);	
		}
		else
		{
			$invoiceJson = $this->apiClient->getJson(
				'/api/v1/invoices/' . $invoiceResumeJson->id
			);	
		}
		$invoice = Invoice::updateOrCreate(
			['id' => $invoiceResumeJson->id],
			[
				'project_id' => $invoiceJson->project_id,
				'cancel' => $canceled,
				'estimation_id' => $canceled ? null : $invoiceJson->estimation_id,
				'invoice_id' => $canceled ? $invoiceJson->invoice_id : null,
				'status'  => $invoiceJson->status,
				'type' => $invoiceJson->type_,
				'name' => $invoiceJson->name,
				'description' => $invoiceJson->description,
				'date' =>$invoiceJson->date,

				'internal_number' => $invoiceJson->internal_number,
				'payment_conditions' => $invoiceJson->payment_conditions,
				'start_date' => $invoiceJson->start_date,
				'end_date' => $invoiceJson->end_date,
				'total_ht' => $invoiceJson->ht,
				'total_tva' => $invoiceJson->tva,
				'total_ttc' => $invoiceJson->ttc,
				'financial_year' => $invoiceJson->financial_year,
				'exported' => $invoiceJson->exported,
				'endi_created_at' => $invoiceJson->created_at,
				'endi_updated_at' => $invoiceJson->updated_at,
			]
		);
	}
}
