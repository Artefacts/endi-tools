<?php

namespace Tests\Unit\Scrappers\Endi;

use PHPUnit\Framework\TestCase;
use PHPHtmlParser\Dom;
use App\Scrappers\Endi\Pager;

class QuotesTest extends TestCase
{

    /**
     * Not a unit test, but a scrap exploration :-)
     * 
     * File is output of "quotes list" : https://endi.ma.coop/estimations
     */
    public function testDevisFile()
    {
        $file = __DIR__.'/../../../../toto.html';

        $dom = new Dom();
        $dom->loadStr( file_get_contents($file) );
        $rows = $dom->find('.table_container table tbody tr');

        $stats = [
            'devisCount' => 0,
            'status-unvalidated' => 0,
            'signed-status-signed' => 0,
            'signed-status-aborted' => 0,
            'signed-status-sent' => 0,
            'signed-status-waiting' => 0,
            'signed-status-UNKNOW' => 0,
            'companies' => null,
        ];

        $ids = [];
        $companies = [] ;

        foreach( $rows as $row )
        {
            $class = $row->tag->getAttribute('class')->getValue() ;
            if( $class == 'row_recap')
                continue ;
            $stats['devisCount'] ++ ;

            // Statut

            if( strpos( $class, 'status-valid') == false )
                $stats['status-unvalidated'] ++ ;
            else if( strpos( $class, 'signed-status-signed') )
                $stats['signed-status-signed'] ++ ;
            else if( strpos( $class, 'signed-status-waiting') )
                $stats['signed-status-waiting'] ++ ;
            else if( strpos( $class, 'signed-status-aborted') )
                $stats['signed-status-aborted'] ++ ;
            else if( strpos( $class, 'signed-status-sent') )
                $stats['signed-status-sent'] ++ ;
            else
            {
                $stats['signed-status-UNKNOW'] ++ ;
                echo 'UNKNOW: ', $class,"\n";
            }

            // Quote ID

            $id = null ;
            $nodes = $row->find( 'td.col_actions .btn-group a' );
            foreach( $nodes as $node )
            {
                if( preg_match( '#^/estimations/(\d+)$#', $node->tag->getAttribute('href')->getValue(), $m ) )
                {
                    $id = \intval( $m[1] );
                    break;
                }
            }
            if( ! $id )
            {
                echo $nodes,"\n";
            }

            $this->assertGreaterThan( 0, $id );
            $this->assertFalse( isset($ids[$id]) );
            $ids[$id] = 1 ;

            // Company

            $nodes = $row->find( 'td.invoice_company_name' );
            $company = $nodes[0]->innerText() ;
            if( ! isset($companies[$company]))
                $companies[$company] = 1 ;
            else
                $companies[$company] ++ ;

        }
        $stats['companies'] = count( array_keys($companies) );

        $this->assertGreaterThan( 0, $stats['devisCount'] );
        echo "\n",'stats: ', print_r($stats,true), "\n";

    }
}
