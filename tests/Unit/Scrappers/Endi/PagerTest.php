<?php

namespace Tests\Unit\Scrappers\Endi;

use PHPUnit\Framework\TestCase;
use PHPHtmlParser\Dom;
use App\Scrappers\Endi\Pager;

class PagerTest extends TestCase
{
    /**
     * It's first page.
     *
     * @return void
     */
    public function testFirstPage()
    {
        $html = '
        <div class="pager">
            <span aria-label="Page en cours" class="current" title="Page en cours">1</span>
            <a class="btn" href="/estimations?_charset_=UTF-8&amp;__formid__=deform&amp;year=-1&amp;signed_status=all&amp;doctype=both&amp;__start__=ttc%3Amapping&amp;start=&amp;end=&amp;__end__=ttc%3Amapping&amp;__start__=period%3Amapping&amp;__start__=start%3Amapping&amp;date=&amp;__end__=start%3Amapping&amp;__start__=end%3Amapping&amp;date=&amp;__end__=end%3Amapping&amp;__end__=period%3Amapping&amp;company_id=&amp;customer_id=&amp;business_type_id=all&amp;search=&amp;items_per_page=500&amp;submit=submit&amp;page=2">2</a>
            <a class="btn" href="/estimations?_charset_=UTF-8&amp;__formid__=deform&amp;year=-1&amp;signed_status=all&amp;doctype=both&amp;__start__=ttc%3Amapping&amp;start=&amp;end=&amp;__end__=ttc%3Amapping&amp;__start__=period%3Amapping&amp;__start__=start%3Amapping&amp;date=&amp;__end__=start%3Amapping&amp;__start__=end%3Amapping&amp;date=&amp;__end__=end%3Amapping&amp;__end__=period%3Amapping&amp;company_id=&amp;customer_id=&amp;business_type_id=all&amp;search=&amp;items_per_page=500&amp;submit=submit&amp;page=3">3</a>
            <span class="spacer">..</span>
            <a class="btn" href="/estimations?_charset_=UTF-8&amp;__formid__=deform&amp;year=-1&amp;signed_status=all&amp;doctype=both&amp;__start__=ttc%3Amapping&amp;start=&amp;end=&amp;__end__=ttc%3Amapping&amp;__start__=period%3Amapping&amp;__start__=start%3Amapping&amp;date=&amp;__end__=start%3Amapping&amp;__start__=end%3Amapping&amp;date=&amp;__end__=end%3Amapping&amp;__end__=period%3Amapping&amp;company_id=&amp;customer_id=&amp;business_type_id=all&amp;search=&amp;items_per_page=500&amp;submit=submit&amp;page=10">10</a>
            <a class="btn" href="/estimations?_charset_=UTF-8&amp;__formid__=deform&amp;year=-1&amp;signed_status=all&amp;doctype=both&amp;__start__=ttc%3Amapping&amp;start=&amp;end=&amp;__end__=ttc%3Amapping&amp;__start__=period%3Amapping&amp;__start__=start%3Amapping&amp;date=&amp;__end__=start%3Amapping&amp;__start__=end%3Amapping&amp;date=&amp;__end__=end%3Amapping&amp;__end__=period%3Amapping&amp;company_id=&amp;customer_id=&amp;business_type_id=all&amp;search=&amp;items_per_page=500&amp;submit=submit&amp;page=2">&gt;</a>
        </div>
        ';

        $dom = new Dom();
        $dom->loadStr($html);
        $items = $dom->find('div.pager > *');
        $lastPage = Pager::isLastPage($items);

        $this->assertFalse($lastPage);
    }

    /**
     * It's a page but not first and not last.
     *
     * @return void
     */
    public function testAPage()
    {
        $html = '
        <div class="pager">
            <a class="btn" href="/estimations?_charset_=UTF-8&amp;__formid__=deform&amp;year=-1&amp;signed_status=all&amp;doctype=both&amp;__start__=ttc%3Amapping&amp;start=&amp;end=&amp;__end__=ttc%3Amapping&amp;__start__=period%3Amapping&amp;__start__=start%3Amapping&amp;date=&amp;__end__=start%3Amapping&amp;__start__=end%3Amapping&amp;date=&amp;__end__=end%3Amapping&amp;__end__=period%3Amapping&amp;company_id=&amp;customer_id=&amp;business_type_id=all&amp;search=&amp;items_per_page=500&amp;submit=submit&amp;page=2">&lt;</a>
            <a class="btn" href="/estimations?_charset_=UTF-8&amp;__formid__=deform&amp;year=-1&amp;signed_status=all&amp;doctype=both&amp;__start__=ttc%3Amapping&amp;start=&amp;end=&amp;__end__=ttc%3Amapping&amp;__start__=period%3Amapping&amp;__start__=start%3Amapping&amp;date=&amp;__end__=start%3Amapping&amp;__start__=end%3Amapping&amp;date=&amp;__end__=end%3Amapping&amp;__end__=period%3Amapping&amp;company_id=&amp;customer_id=&amp;business_type_id=all&amp;search=&amp;items_per_page=500&amp;submit=submit&amp;page=1">1</a>
            <a class="btn" href="/estimations?_charset_=UTF-8&amp;__formid__=deform&amp;year=-1&amp;signed_status=all&amp;doctype=both&amp;__start__=ttc%3Amapping&amp;start=&amp;end=&amp;__end__=ttc%3Amapping&amp;__start__=period%3Amapping&amp;__start__=start%3Amapping&amp;date=&amp;__end__=start%3Amapping&amp;__start__=end%3Amapping&amp;date=&amp;__end__=end%3Amapping&amp;__end__=period%3Amapping&amp;company_id=&amp;customer_id=&amp;business_type_id=all&amp;search=&amp;items_per_page=500&amp;submit=submit&amp;page=2">2</a>
            <span aria-label="Page en cours" class="current" title="Page en cours">3</span>
            <a class="btn" href="/estimations?_charset_=UTF-8&amp;__formid__=deform&amp;year=-1&amp;signed_status=all&amp;doctype=both&amp;__start__=ttc%3Amapping&amp;start=&amp;end=&amp;__end__=ttc%3Amapping&amp;__start__=period%3Amapping&amp;__start__=start%3Amapping&amp;date=&amp;__end__=start%3Amapping&amp;__start__=end%3Amapping&amp;date=&amp;__end__=end%3Amapping&amp;__end__=period%3Amapping&amp;company_id=&amp;customer_id=&amp;business_type_id=all&amp;search=&amp;items_per_page=500&amp;submit=submit&amp;page=4">4</a>
            <a class="btn" href="/estimations?_charset_=UTF-8&amp;__formid__=deform&amp;year=-1&amp;signed_status=all&amp;doctype=both&amp;__start__=ttc%3Amapping&amp;start=&amp;end=&amp;__end__=ttc%3Amapping&amp;__start__=period%3Amapping&amp;__start__=start%3Amapping&amp;date=&amp;__end__=start%3Amapping&amp;__start__=end%3Amapping&amp;date=&amp;__end__=end%3Amapping&amp;__end__=period%3Amapping&amp;company_id=&amp;customer_id=&amp;business_type_id=all&amp;search=&amp;items_per_page=500&amp;submit=submit&amp;page=5">5</a>
            <span class="spacer">..</span>
            <a class="btn" href="/estimations?_charset_=UTF-8&amp;__formid__=deform&amp;year=-1&amp;signed_status=all&amp;doctype=both&amp;__start__=ttc%3Amapping&amp;start=&amp;end=&amp;__end__=ttc%3Amapping&amp;__start__=period%3Amapping&amp;__start__=start%3Amapping&amp;date=&amp;__end__=start%3Amapping&amp;__start__=end%3Amapping&amp;date=&amp;__end__=end%3Amapping&amp;__end__=period%3Amapping&amp;company_id=&amp;customer_id=&amp;business_type_id=all&amp;search=&amp;items_per_page=500&amp;submit=submit&amp;page=10">10</a>
            <a class="btn" href="/estimations?_charset_=UTF-8&amp;__formid__=deform&amp;year=-1&amp;signed_status=all&amp;doctype=both&amp;__start__=ttc%3Amapping&amp;start=&amp;end=&amp;__end__=ttc%3Amapping&amp;__start__=period%3Amapping&amp;__start__=start%3Amapping&amp;date=&amp;__end__=start%3Amapping&amp;__start__=end%3Amapping&amp;date=&amp;__end__=end%3Amapping&amp;__end__=period%3Amapping&amp;company_id=&amp;customer_id=&amp;business_type_id=all&amp;search=&amp;items_per_page=500&amp;submit=submit&amp;page=4">&gt;</a>
        </div>
        ';

        $dom = new Dom();
        $dom->loadStr($html);
        $items = $dom->find('div.pager > *');
        $lastPage = Pager::isLastPage($items);

        $this->assertFalse($lastPage);
    }


    /**
     * It's a page but not first and not last.
     *
     * @return void
     */
    public function testNoPage()
    {
        $html = '
        <div class="pager">
        </div>
        ';

        $dom = new Dom();
        $dom->loadStr($html);
        $items = $dom->find('div.pager > *');
        $lastPage = Pager::isLastPage($items);

        $this->assertTrue($lastPage);
    }

    /**
     * It's a page but not first and not last.
     *
     * @return void
     */
    public function testLastPage()
    {
        $html = '
        <div class="pager">
            <a class="btn" href="/estimations?_charset_=UTF-8&amp;__formid__=deform&amp;year=-1&amp;signed_status=all&amp;doctype=both&amp;__start__=ttc%3Amapping&amp;start=&amp;end=&amp;__end__=ttc%3Amapping&amp;__start__=period%3Amapping&amp;__start__=start%3Amapping&amp;date=&amp;__end__=start%3Amapping&amp;__start__=end%3Amapping&amp;date=&amp;__end__=end%3Amapping&amp;__end__=period%3Amapping&amp;company_id=&amp;customer_id=&amp;business_type_id=all&amp;search=&amp;items_per_page=500&amp;submit=submit&amp;page=9">&lt;</a>
            <a class="btn" href="/estimations?_charset_=UTF-8&amp;__formid__=deform&amp;year=-1&amp;signed_status=all&amp;doctype=both&amp;__start__=ttc%3Amapping&amp;start=&amp;end=&amp;__end__=ttc%3Amapping&amp;__start__=period%3Amapping&amp;__start__=start%3Amapping&amp;date=&amp;__end__=start%3Amapping&amp;__start__=end%3Amapping&amp;date=&amp;__end__=end%3Amapping&amp;__end__=period%3Amapping&amp;company_id=&amp;customer_id=&amp;business_type_id=all&amp;search=&amp;items_per_page=500&amp;submit=submit&amp;page=1">1</a>
            <span class="spacer">..</span>
            <a class="btn" href="/estimations?_charset_=UTF-8&amp;__formid__=deform&amp;year=-1&amp;signed_status=all&amp;doctype=both&amp;__start__=ttc%3Amapping&amp;start=&amp;end=&amp;__end__=ttc%3Amapping&amp;__start__=period%3Amapping&amp;__start__=start%3Amapping&amp;date=&amp;__end__=start%3Amapping&amp;__start__=end%3Amapping&amp;date=&amp;__end__=end%3Amapping&amp;__end__=period%3Amapping&amp;company_id=&amp;customer_id=&amp;business_type_id=all&amp;search=&amp;items_per_page=500&amp;submit=submit&amp;page=8">8</a>
            <a class="btn" href="/estimations?_charset_=UTF-8&amp;__formid__=deform&amp;year=-1&amp;signed_status=all&amp;doctype=both&amp;__start__=ttc%3Amapping&amp;start=&amp;end=&amp;__end__=ttc%3Amapping&amp;__start__=period%3Amapping&amp;__start__=start%3Amapping&amp;date=&amp;__end__=start%3Amapping&amp;__start__=end%3Amapping&amp;date=&amp;__end__=end%3Amapping&amp;__end__=period%3Amapping&amp;company_id=&amp;customer_id=&amp;business_type_id=all&amp;search=&amp;items_per_page=500&amp;submit=submit&amp;page=9">9</a>
            <span aria-label="Page en cours" class="current" title="Page en cours">10</span> 
        </div>
        ';

        $dom = new Dom();
        $dom->loadStr($html);
        $items = $dom->find('div.pager > *');
        $lastPage = Pager::isLastPage($items);

        $this->assertTrue($lastPage);
    }
}
