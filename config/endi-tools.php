<?php

return [
	'base_url' => env('ENDITOOLS_SITE'),
	'username' => env('ENDITOOLS_USER'),
	'password' => env('ENDITOOLS_PWD'),

	'web_user_agent' => env('ENDITOOLS_USERAGENT', 'Mozilla/5.0 (X11; Linux x86_64; rv:130.0) Gecko/20100101 Firefox/130.0'),
	'api_user_agent' => 'artefacts/endi-tools',

	// Sleep milliseconds between api call, default to 1 second.
	// With 500 ms is enough to get you stuck quickly.
	'http_sleep' => env('ENDITOOLS_HTTP_SLEEP', 1000),

	'cache' => [
		/*
		to avoid overloading the enDI server
		manage a file cache for HTTP requests
		*/
		'active' => env('ENDITOOLS_CACHE', true),
		'folder' => env('ENDITOOLS_CACHE_FOLDER', storage_path('app/cache/')),
		// Default to 1 hour
		'ttl' => env('ENDITOOLS_CACHE_TTL', 3600),
	],

];