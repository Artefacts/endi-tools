# enDI Tools

Scrap des pages ou utilise l'API de l'application [enDI](https://framagit.org/endi/endi) pour fournir quelques services.

## Services


### commande "enditools:updateDatabase"

Utilise l'API pour récupérer des données enDI.

Un cache est activé par défaut avec une durée de 1 heure pour éviter la surcharge du serveur.

Les tables alimentées sont:
- `companies`: les enseignes
- `projects`: une meta structure pour retrouver les devis, factures et autres
- `estimations`: les devis
- `invoices`: les factures

### commande "enditools:scrape"

Surement obsolète

## Roadmap

- [WIP] 1er pas
- [WIP] liste des ESA
    - Utiliser l'export généré en tâche de fond
- [WIP] Notification périodique avec la liste des devis signés
    - si marqué "signé" et "pièce jointe"
    - changement de statut possible : signé -> non-signé
    - Comment détecté les derniers modifiés : seuleent en chargeant tout et comparer avec version précédente :-(

## Tech

En Php avec composer.

### Discussions

- agora Fédé CAE
  - https://forum.endi.coop/t/api-rest-pour-joindre-endi/1672
- Framagit enDI
  - API Rest : spécifications https://framagit.org/caerp/caerp/-/issues/3633
  - API https://framagit.org/caerp/caerp/-/issues/3534
  - Améliorer l'authentification par clé d'api https://framagit.org/caerp/caerp/-/issues/686
- Projet yoandm/phpEndi
  - https://github.com/yoandm/phpEndi/issues/2

### API

Il n'y a souvent pas d'index car la plupart des apis sont conçus pour l'Ajout/modification de document ou pour un accès depuis une enseigne.

Pour ce qui est du code, il est basé sur le framework Pyramid.

### Obtenir du JSON

Une fois connecté en web
on change l'url en ajoutant `api/v1`

`https://endi.coop/companies/56644/customers`
devient `https://endi.coop/api/v1/companies/56644/customers`

Et les PUT et POST fonctionnent aussi :-) pour certains endpoint seulement :-(.

