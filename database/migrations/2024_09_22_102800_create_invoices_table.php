<?php

use App\Models\Estimation;
use App\Models\Invoice;
use App\Models\Project;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( Invoice::TABLENAME, function (Blueprint $table) {
            $table->id();
            $table->integer('project_id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->integer('financial_year');
            $table->string('type');
            $table->string('status');
            $table->datetime('date');
            $table->string('official_number')->nullable();
            $table->string('internal_number');
            $table->text('payment_conditions')->nullable();
            $table->datetime('start_date')->nullable();
            $table->datetime('end_date')->nullable();
            $table->decimal('total_ht');
            $table->decimal('total_tva');
            $table->decimal('total_ttc');
            $table->boolean('exported')->comment('export compta ?');

            $table->boolean('cancel')->comment('Canceled invoice: no estimation_id but invoice_id');
            $table->integer('estimation_id')->nullable();
            $table->integer('invoice_id')->nullable();

            $table->datetime('endi_created_at');
            $table->datetime('endi_updated_at');
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on( Project::TABLENAME );

            // the order in which the data arrives does not always allow foreign keys to be assigned.
            //
            //$table->foreign('estimation_id')->references('id')->on( Estimation::TABLENAME );
            //$table->foreign('invoice_id')->references('id')->on( Invoice::TABLENAME );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Invoice::TABLENAME);
    }
}
