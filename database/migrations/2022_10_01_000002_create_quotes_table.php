<?php

use App\Models\Company;
use App\Models\Quote;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( Quote::TABLENAME, function (Blueprint $table) {
            $table->id();
            $table->integer('company_id');
            $table->string('status');
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on( Company::TABLENAME );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Quote::TABLENAME);
    }
}
