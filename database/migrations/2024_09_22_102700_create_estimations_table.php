<?php

use App\Models\Company;
use App\Models\Estimation;
use App\Models\Project;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstimationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( Estimation::TABLENAME, function (Blueprint $table) {
            $table->id();
            $table->integer('project_id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('status');
            $table->datetime('date');
            $table->string('official_number')->nullable();
            $table->string('internal_number');
            $table->text('payment_conditions')->nullable();
            $table->datetime('start_date')->nullable();
            $table->datetime('end_date')->nullable();
            $table->decimal('total_ht');
            $table->decimal('total_tva');
            $table->decimal('total_ttc');
            $table->integer('payment_lines_count');
            $table->datetime('endi_created_at');
            $table->datetime('endi_updated_at');
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on( Project::TABLENAME );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Estimation::TABLENAME);
    }
}
